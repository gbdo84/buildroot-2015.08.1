About:
Itoy is a light-weight linux system for Beagobone Board Black that uses Buildroot tool.


Instructions:

1. getting repository:
git clone git@gitlab.com:gbdo84/buildroot-2015.08.1.git

2. Plattform setup:
make itoy_defconfig

3. Building Project:
make

4. SDcard setup (for /dev/sdb, otherwise we need change deploy rule in Makefile buildroot):
make format_sdcard. We should ignore error messages and mountthe partitions.

5. Writting sdcard and deploy image at sdcard:
make deploy

6. usb-ethernet interface has IP 192.168.2.1/24 and ssh server, login root, without password.

7. RS-232 degub: take a look at docs/images/bbb_console_how_to.jpg to get properly cable assembling.


